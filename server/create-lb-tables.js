//use this javascript with caution only use it if the database misses the ACL table. This will delete the current ACL if it exist.CAUTIONS
var server = require('./server');
var ds = server.dataSources.rethinkdb;
var lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role'];
ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' - lbTables - '] created in ', ds.adapter.name);
  ds.disconnect();
});