
'use strict';

module.exports = function(User) {
  User.afterRemote('replaceOrCreate', function(ctx, userInstance, next) {

    /**
     * The container is the name of the folder which hold the information
     * The the user id is the name of the container created.
     * @type {{name: String }}
     */
    var container = {
      name: userInstance.id.toString()
    };
    console.log(container.name);

    /**
     * Passing in the container model to the storage class to create container for the user.
     */
    User.app.models.storage.createContainer(container, function(error, response) {
      if (error) return console.error("Couldn't create container for the user " + userInstance.id);
      console.log(response);
    });
    next();
  });

  



  //todo : Test if other user can delete other person profile pic in the container.
};
